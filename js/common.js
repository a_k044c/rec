$(function(){

//--------------------------------------
//  gloval navigation
//--------------------------------------
  $('.side_mainNav li').attr('class','side_mainNav_item');

  $('.side_mainNav_item').mouseover(function() {
    $('.side_subNav').addClass('open');
    $('.side_subNav ul').removeClass('open');
    $(this).addClass('is-on');
    $('.side_mainNav_item').not($(this)).removeClass('is-on');

    var num_list = $('.side_mainNav_item').index(this) + 1;
    if($(this).hasClass('is-on')){
      $('.side_subNav ul:nth-'+ 'child(' + num_list  +')').addClass('open');
    } else {
      $('.side_subNav ul:nth-'+ 'child(' + num_list  +')').removeClass('open');
      }
    });

    $('article,header').mouseover(function() {
      $('.side_subNav,.side_subNav ul').removeClass('open');
      $('.side_mainNav_item').removeClass('is-on');
    });
});
